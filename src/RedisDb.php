<?php
namespace OrangeMan\Db;

/**
 * redis缓存单例操作类
 * Class redisDb
 */
class RedisDb
{
    private $host = '';
    private $password = '';
    private $port = '';
    private $prefix = '';
    private $dbIndex = 0;
    private $redis;
    static $_instance;

    /**
     * 初始化
     * RedisDb constructor.
     * @param array $config
     */
    private function __construct($config = [])
    {
        $this->host = $config['host'];
        $this->port = $config['port'];
        if (isset($config['password'])) {
            $this->password = $config['password'];
        }
        if (isset($config['prefix'])) {
            $this->prefix = $config['prefix'];
        }
        if (isset($config['db_index'])) {
            $this->dbIndex = $config['db_index'];
        }
        $this->redis = new \Redis();
        try {
            $this->redis->connect($this->host, $this->port);
            if ($this->password) {
                $this->redis->auth($this->password);//密码验证
            }
            // 选择数据库默认0，默认16个数据库可以配置多个
            $this->redis->select($this->dbIndex);
        } catch (Exception $e) {
            throw new Exception('redis服务器连接失败');
        }
        return $this->redis;
    }

    private function __clone()
    {

    }

    /**
     * 获取实例
     * @param $config
     * @return RedisDb
     */
    public static function getInstance($config)
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self($config);
        }
        return self::$_instance;
    }

    /**
     * 设置缓存
     * @param $key
     * @param $value
     * @param int $expiresTime 0 永久
     * @return bool
     */
    public function set($key, $value, $expiresTime = 0)
    {
        $key = $this->getPrefixKey($key);
        return $this->redis->set($key, $value, $expiresTime);
    }

    /**
     * 获取缓存
     * @param $key
     * @return bool|string
     */
    public function get($key)
    {
        $key = $this->getPrefixKey($key);
        return $this->redis->get($key);
    }

    /**
     * 删除缓存
     * @param $key
     * @return int
     */
    public function del($key)
    {
        $key = $this->getPrefixKey($key);
        return $this->redis->del($key);
    }

    /**
     * 删除所有缓存
     * @return bool
     */
    public function flushAll()
    {
        return $this->redis->flushAll();
    }

    /**
     * hash set
     * @param $key
     * @param $hashKey
     * @param $value
     * @return bool|int
     */
    public function hSet($key, $hashKey, $value)
    {
        $key = $this->getPrefixKey($key);
        return $this->redis->hSet($key, $hashKey, $value);
    }

    /**
     * hash get
     * @param $key
     * @param $hashKey
     * @return string
     */
    public function hGet($key, $hashKey)
    {
        $key = $this->getPrefixKey($key);
        return $this->redis->hGet($key, $hashKey);
    }

    /**
     * hash del
     * @param $key
     * @param $hashKeyArray
     * @return bool|int
     */
    public function hDel($key, $hashKeyArray)
    {
        $key = $this->getPrefixKey($key);
        if (!is_array($hashKeyArray)) {
            return false;
        }
        $hashKeyString = implode(',', $hashKeyArray);
        return $this->redis->hDel($key, $hashKeyString);
    }

    /**
     * hash get all
     * @param $key
     * @return array
     */
    public function hGetAll($key)
    {
        $key = $this->getPrefixKey($key);
        return $this->redis->hGetAll($key);
    }

    /**
     * 队列处理-入栈
     * @param $key
     * @param $value
     * @return int
     */
    public function rPush($key, $value)
    {
        $key = $this->getPrefixKey($key);
        return $this->redis->rPush($key, $value);
    }

    /**
     * 队列处理-出栈
     * @param $key
     * @return string
     */
    public function lPop($key)
    {
        $key = $this->getPrefixKey($key);
        return $this->redis->lPop($key);
    }

    /**
     * 查看队列情况
     * @param $key
     * @param int $start
     * @param int $end
     * @return array
     */
    public function lRange($key, $start = 0, $end = -1)
    {
        $key = $this->getPrefixKey($key);
        return $this->redis->lRange($key, $start, $end);
    }

    /**
     * 自增 1
     * @param $key
     * @return int
     */
    public function incr($key)
    {
        $key = $this->getPrefixKey($key);
        return $this->redis->incr($key);
    }

    /**
     * 自减 1
     * @param $key
     * @return int
     */
    public function decr($key)
    {
        $key = $this->getPrefixKey($key);
        return $this->redis->decr($key);
    }

    /**
     * 缓存操作
     * @param $key
     * @param string $value
     * @param int $expiresTime
     * @return bool|mixed
     */
    public function cache($key, $value = '', $expiresTime = 0)
    {
        $key = $this->getPrefixKey($key);
        // 设置缓存
        if ($key && ($value || is_array($value))) {
            $value = json_encode($value);
            return $this->redis->set($key, $value, $expiresTime);
        }
        // 获取当前key缓存
        if ($key && $value === '') {
            return json_decode($this->redis->get($key), true);
        }
        // 删除当前key缓存
        if ($key != $this->getPrefixKey('all') && is_null($value)) {
            return $this->redis->del($key);
        }
        // 删除所有缓存
        if ($key == $this->getPrefixKey('all') && is_null($value)) {
            return $this->redis->flushAll();
        }
        return false;
    }

    /**
     * 获取缓存前缀
     * @param $key
     * @return string
     */
    private function getPrefixKey($key)
    {
        $keyPrefix = $this->prefix ? $this->prefix : '';
        return $keyPrefix . $key;
    }
}